from functools import cmp_to_key
from operator import attrgetter

class Queue:
    _queue = []
    _count = 0

    @property
    def queue(self):
        """Returns sorted event of itself"""
        self._queue.sort(key=cmp_to_key(self.__orderer__))
        return self._queue

    @staticmethod
    def __orderer__(a, b):
        return -1 if a < b else 1

    def __len__(self):
        return len(_queue)

    def __iter__(self):
        for i in self.queue:
            yield i

    def __reversed__(self):
        for i in reversed(self.queue):
            yield i

    def __contains__(self, item):
        return item in self._queue

    def __init__(self):
        pass

    def index(self, vid):
        self.queue.index(vid)

    def __repr__(self):
        return 'current count: {}, {}'.format(self._count, self.queue)

    def dict(self):
        return {"res": [v.dict() for v in self.queue]}

    def __list__(self):
        return self.queue

    def put(self, video):
        """Add video to queue"""
        if video in self._queue:
            # TODO: Test that this goes by == and not memory pos
            self._queue[self._queue.index(video)] += video
        else:
            video.set_count(self._count)
            self._count += 1
            self._queue.append(video)

    def pop(self):
        return self.queue.pop(0)

    def update_dir(self):
        return {"queue": [x.update_dir() for x in self]}

    def get(self):
        """Get highest voted element from queue"""
        return self.queue[0]

    def toggle_video_vote(self, video_id, usid):
        for item in self._queue:
            if item.cmp(video_id):
                if item.toggle_vote(usid):
                    self._queue.remove(item)
                break

    def fetch_all(self):
        return self.queue
