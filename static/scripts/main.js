$(document).ready(function() {

    var socket = io.connect('http://' + document.domain + ':' + location.port + '/');
    // TODO: set player seek to false when launching
    var player = document.getElementById('audio');
    var playing = true;
    var queued = false;
    var first = true;
    function usid_in_array(a) {
        return a.indexOf($.cookie("usid")) != -1;
    }

    if($.cookie("usid") != null) {
      $("#usid_info").text($.cookie("usid"));
    }

    socket.on('connect', function() {

        $('#btn_search').click(function() {
            if ($('#search_term').val() != '') {
                console.log("Button pressed");
                var query = $('#search_term').val();
                // send data to socket
                socket.emit('find_search', {data: query, usid: $.cookie("usid")} );
                $('#search_term').val('')
            }
        });
        $('#search_term').keypress(function( e ) {
            if(e.which == 13) { // 13 = enter_key
                if ($('#search_term').val() != '') {
                    console.log("Enter pressed");
                    var query = $('#search_term').val();
                    // send data to socket
                    socket.emit('find_search', {data: query, usid: $.cookie("usid")} );
                    $('#search_term').val('')
                }
            }
        });
    });

    socket.on('list_search', function( videos ) {
        $('.list-group-item').remove();
        for (let i = 0; i < videos['res'].length; i++) {
            $('#search_list').append(
              '<button type="button" class="list-group-item" id=' + i + '>' +
              videos['res'][i]['title'] + '</button>');
            $('#' + i).click(function() {
              // TODO change below
              socket.emit('search_result', {'id': i, usid: $.cookie("usid")});
            });
        }

    });

    socket.on('update_queue', function( queue ) {
        // TODO: remove from queue when currently playing
        if(player != null) {
          if(!queued && queue['queue'].length == 1 && (player.ended || first)) {
            first = false;
            queued = true;
            socket.emit('fetch_next_song');
            return;
          }
        }
        $('.ul-media').empty();
        for (let i = 0; i < queue['queue'].length; i++) {
            $('.ul-media').append(create_queue_item(queue['queue'][i], i))
            var votes = queue['queue'][i]['votes']; // change to let??
            var song = queue['queue'][i];
            if(usid_in_array(queue['queue'][i]['usids'])) {
                $('.btn-vote').text("Unvote " + votes);
                // Not working, need to look at this later
                // Currently updating all the items in the queue related to the user
                $(".btn-vote").on("click", function() {
                  socket.emit('reduce_vote', $.cookie("usid"), song);
                  console.log("Clicked the button");
                });
            }
            else {
              socket.emit('increase_vote', votes, $.cookie("usid"), song);
            }
        }
    });

    socket.on('play_next_song', function( song ) {
        player.src = song['url'];
        $(".song_title").text(song['title']);
        $(".currently_playing_photo").attr("src",song['thumbnail']);
        player.play();
        playing = true;
        $(player).on('ended', function() {
            if(playing) {
		            queued = false;
                console.log('Song has ended, fetching next song from queue...');
                player.pause();
                playing = false;
                socket.emit('fetch_next_song');
            }
        })
    });

    function create_queue_item(queue_item, i) {
      console.log(queue_item);
      html =  '<li class="media">' +
                '<button class="btn btn-warning btn-vote">Vote: ' + queue_item['votes'] + '</button>' +
                '<img class="mr-3 queue_photo" src="' + queue_item['thumb']['url'] + '" alt="Thumbnail">' +
                '<div class="media-body">' +
                  '<h5 class="mt-0 mb-1">' + queue_item['title'] +  '</h5>' +
                  '<p class="queue_subtitle"> --:-- / SONG_LENGTH </p>' +
                '</div>' +
              '</li>';
      return html;
    }

    player.ontimeupdate = function() {
      var current_minutes = Math.floor(player.currentTime / 60);
      var current_seconds = Math.floor(player.currentTime % 60);
      $("#progress_bar").css('width', player.currentTime / player.duration * 100 + '%');
      $(".subtitle").text("" + current_minutes + " : " + current_seconds + " / " + Math.floor(player.duration / 60) + " : " + Math.floor(player.duration % 60));
    }

    socket.on('queue response', function( results ) {
      var r = JSON.Parse(results);
      $('li').remove();
      for(const k of r) {
        console.log(k);
          $('#search_list').append('<li>' + k.video_title + '</li>');
      }
    });

});
