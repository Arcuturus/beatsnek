$(document).ready(function() {

    var socket = io.connect('http://' + document.domain + ':' + location.port + '/');
    // TODO: set player seek to false when launching
    var player = document.getElementById('audio');
    var playing = true;
    var queued = false;
    var first = true;

    socket.on('update_queue', function( queue ) {
        // TODO: remove from queue when currently playing
        if(player != null) {
          if(!queued && queue['queue'].length == 1 && (player.ended || first)) {
            first = false;
            queued = true;
            socket.emit('fetch_next_song');
            return;
          }
        }
        $('.ul-media').empty();
        for (let i = 0; i < queue['queue'].length; i++) {
            $('.ul-media').append(create_queue_item(queue['queue'][i], i))
            var votes = queue['queue'][i]['votes']; // change to let??
            var song = queue['queue'][i];
        }
    });

    socket.on('play_next_song', function( song ) {
        player.src = song['url'];
        $(".song_title").text(song['title']);
        $(".currently_playing_photo").attr("src",song['thumbnail']);
        player.play();
        playing = true;
        $(player).on('ended', function() {
            if(playing) {
		            queued = false;
                console.log('Song has ended, fetching next song from queue...');
                player.pause();
                playing = false;
                socket.emit('fetch_next_song');
            }
        })
    });

    function create_queue_item(queue_item, i) {
      console.log(queue_item);
      html =  '<li class="media">' +
                '<img class="mr-3 queue_photo" src="' + queue_item['thumb']['url'] + '" alt="Thumbnail">' +
                '<div class="media-body">' +
                  '<h5 class="mt-0 mb-1">' + queue_item['title'] +  '</h5>' +
                  '<p class="queue_subtitle"> --:-- / SONG_LENGTH </p>' +
                '</div>' +
              '</li>';
      return html;
    }

    player.ontimeupdate = function() {
      var current_minutes = Math.floor(player.currentTime / 60);
      var current_seconds = Math.floor(player.currentTime % 60);
      $("#progress_bar").css('width', player.currentTime / player.duration * 100 + '%');
      $(".subtitle").text("" + current_minutes + " : " + current_seconds + " / " + Math.floor(player.duration / 60) + " : " + Math.floor(player.duration % 60));
    }

});
