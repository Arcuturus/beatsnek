$(document).ready(function() {

    var socket = io.connect('http://' + document.domain + ':' + location.port + '/');
    // TODO: set player seek to false when launching
    function usid_in_array(a) {
        return a.indexOf($.cookie("usid")) != -1;
    }

    socket.on('connect', function() {

      socket.emit('get_queue');

        $('#btn_search').click(function() {
            if ($('#search_term').val() != '') {
                console.log("Button pressed");
                var query = $('#search_term').val();
                // send data to socket
                socket.emit('find_search', {data: query, usid: $.cookie("usid")} );
                $('#search_term').val('')
            }
        });
        $('#search_term').keypress(function( e ) {
            if(e.which == 13) { // 13 = enter_key
                if ($('#search_term').val() != '') {
                    console.log("Enter pressed");
                    var query = $('#search_term').val();
                    // send data to socket
                    socket.emit('find_search', {data: query, usid: $.cookie("usid")} );
                    $('#search_term').val('')
                }
            }
        });
    });

    socket.on('list_search', function( videos ) {
        $('.list-group-item').remove();
        for (let i = 0; i < videos['res'].length; i++) {
            $('#search_list').append(
              '<button type="button" class="list-group-item" id=' + i + '>' +
              videos['res'][i]['title'] + '</button>');
            $('#' + i).click(function() {
              // TODO change below
              socket.emit('search_result', {'id': i, usid: $.cookie("usid")});
            });
        }

    });

    socket.on('update_queue', function( queue ) {
        $('.ul-media').empty();
        for (let i = 0; i < queue['queue'].length; i++) {
            $('.ul-media').append(create_queue_item(queue['queue'][i], i))
            let video_id = queue['queue'][i]['id'];
            let votes = queue['queue'][i]['votes'];
            if(usid_in_array(queue['queue'][i]['usids'])) {
                $('#list_id' + i).removeClass('btn-success').addClass('btn-warning');
                $('#list_id' + i).text("Unvote " + votes);
            }
            $("#list_id" + i).on("click", function() {
                socket.emit('toggle_vote', $.cookie("usid"), video_id);
                console.log("Clicked the button");
            });
        }
    });

    function create_queue_item(queue_item, i) {
      console.log(queue_item);
      html =  '<li class="media">' +
                '<button class="btn btn-success btn-vote" id="list_id' + i + '">Vote: ' + queue_item['votes'] + '</button>' +
                '<img class="mr-3 queue_photo" src="' + queue_item['thumb']['url'] + '" alt="Thumbnail">' +
                '<div class="media-body">' +
                  '<h5 class="mt-0 mb-1">' + queue_item['title'] +  '</h5>' +
                  '<p class="queue_subtitle"> --:-- / SONG_LENGTH </p>' +
                '</div>' +
              '</li>';
      return html;
    }

});
