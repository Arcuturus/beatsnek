import unittest
from pqueue import Queue as q
from yt_api import Video

test_values = [
    {'id':{'videoId': 0}, 'snippet': {'title': 0, 'thumbnails': {'high' : ''}}},
    {'id':{'videoId': 1}, 'snippet': {'title': 1, 'thumbnails': {'high' : ''}}},
    {'id':{'videoId': 2}, 'snippet': {'title': 2, 'thumbnails': {'high' : ''}}},
    {'id':{'videoId': 3}, 'snippet': {'title': 3, 'thumbnails': {'high' : ''}}},
    {'id':{'videoId': 4}, 'snippet': {'title': 4, 'thumbnails': {'high' : ''}}},
    {'id':{'videoId': 5}, 'snippet': {'title': 5, 'thumbnails': {'high' : ''}}},
]

class PQueueTest(unittest.TestCase):

    def test_queue_add_and_get_sorted(self):
        queue = q()
        videos = [Video(x) for x in test_values]
        test_vars = [
            {'votes': 2, 'pos': 0},
            {'votes': 2, 'pos': 1},
            {'votes': 3, 'pos': 2},
            {'votes': 3, 'pos': 3},
            {'votes': 1, 'pos': 4},
            {'votes': 4, 'pos': 5},
        ]

        for v, p in zip(videos, test_vars):
            v.votes = p['votes']

        for i in videos:
            queue.put(i)

        self.assertEqual(len(queue._queue), 6)

        print(queue)
        
        res = {i: queue.pop() for i in range(6)}
        expected = {0: 5, 1: 2, 2: 3, 3: 0, 4: 1, 5: 4}
        for k, v in expected.items():
            self.assertEqual(
                res[k].video_id,
                v,
                "pos {}, {} is expected, we got {}".format(k, v, res[k].video_id)
            )
