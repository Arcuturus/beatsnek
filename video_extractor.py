import youtube_dl as ytdl

class VideoExtractor():

    def __init__(self, video_id):
        self.video_url = video_id
        self.ydl_opts = {
            'format': 'bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'mp4',
                'preferredquality': '720',
            }],
        }
        self.ydl = ytdl.YoutubeDL(self.ydl_opts)
        self.base_url = 'http://www.youtube.com/watch?v='
        self.get_video_from_url(self.video_url)

    def get_video_from_url(self, url):
        try:
            self.match = self.ydl.extract_info(
                '{}{}'.format(self.base_url,self.video_url),
                download=False
            )
        except ytdl.utils.DownloadError as de:
            print(de)
            return False

    def fetch_video(self):
        if 'entries' in self.match:
            return self.match['entries'][0]
        else:
            return self.match
