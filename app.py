from __future__ import unicode_literals

from flask import Flask, render_template, request, make_response
from flask_socketio import SocketIO, emit
import json
import random
import string
import logging

from video_extractor import VideoExtractor as ve
from pqueue import Queue as PlayQueue
from yt_api import YouTubeSearch

users = {}
player = {}

app = Flask(__name__)
app.config['SECRET_KEY'] = 'SUPER_SECRET_KEY_I_PROMISE'
socketio = SocketIO(app)

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

yts = YouTubeSearch()

play_queue = PlayQueue()

def generate_user_id():
    id = ''.join(random.choices(string.ascii_letters + string.digits, k=20))
    if not id in users.keys():
        users[id] = {}
        return id
    else:
        return generate_user_id()

def ack(val):
    print('Ok from {}'.format(val))


def get_video(search_term):
    return yts.search(search_term)

@app.route('/player')
def index():

    cookie = request.cookies.get('playerid')
    response = make_response(render_template('player.html'))
    if not cookie:
        cookie = generate_user_id()
        response.set_cookie('playerid', cookie)
    elif cookie not in users.keys():
        users[cookie] = {}

    global player

    if not player:
        player = {"playerid": cookie}
        return response
    return render_template("403.html"), 403

@app.route('/')
def search_page():
    cookie = request.cookies.get('usid')
    response = make_response(render_template('index.html'))

    if not cookie:
        cookie = generate_user_id()
        response.set_cookie('usid', cookie)
    elif cookie not in users.keys():
        users[cookie] = {}

    return response

def userid_present(f):
    def wrapper(*args, **kwargs):
        if args[0]['usid'] not in users.keys():
            print(args, users, sep='\n')
            print("I FAULTED YOUR PLAN")
            return render_template('403.html'), 403
        f(*args, **kwargs)
    return wrapper

@socketio.on('get_queue')
def get_queue():
    return emit("update_queue", play_queue.update_dir(), callback=ack("get_queue"))


@socketio.on('find_search', namespace='/')
@userid_present
def query_search(query):
    """
    Query the search from the user and display it to them
    """
    print(query)
    # hopefully this never runs....
    #if query['usid'] not in users.keys():
    #    return render_template('404.html'), 404

    users[query['usid']]['search'] = get_video(query['data'])
    dict_videos = {'res': [x.dict() for x in users[query['usid']]['search']]}

    # TODO: pass usid so that only the searching user get the results, not all
    emit('list_search', dict_videos, callback=ack("list_search"))

@socketio.on('search_result')
@userid_present
def add_search_results(query):

    if query['usid'] not in users.keys():
        return render_template('404.html'), 404

    vid = users[query['usid']]['search'][query['id']]
    if vid.add_vote(query['usid']):
        play_queue.put(vid)

        emit("update_queue", play_queue.update_dir(), broadcast=True, callback=ack("update_queue"))


@socketio.on('fetch_next_song')
def fetch_next_song():
    try:
        song = play_queue.pop().video_id
        print(play_queue)
        item = ve(song)
        if item:
            emit('play_next_song', item.fetch_video(), callback=ack("play_next_song"))
            emit("update_queue", play_queue.update_dir(), broadcast=True, callback=ack("update_queue_after_song"))
        else:
            print("Download failed!??!?")
            fetch_next_song()

    except AttributeError as ie:
        print(ie)

@socketio.on('toggle_vote')
def reduce_vote(usid, video_id):
    play_queue.toggle_video_vote(video_id, usid)
    emit("update_queue", play_queue.update_dir(), broadcast=True, callback=ack("update_queue_after_toggling_vote"))


@app.errorhandler(404)
def page_not_found_exception(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def page_forbidden_exception(e):
    return render_template('403.html'), 403



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
